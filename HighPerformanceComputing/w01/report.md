------- TASK 1 --------

1.     PATH   The  search  path for commands.  It is a colon-separated list of
              directories in which the shell looks for commands  (see  COMMAND
              EXECUTION  below).   A  zero-length (null) directory name in the
              value of PATH indicates the current directory.  A null directory
              name  may  appear  as  two  adjacent colons, or as an initial or
              trailing colon.  The default path is  system-dependent,  and  is
              set  by  the administrator who installs bash.  A common value is
              ``/usr/gnu/bin:/usr/local/bin:/usr/ucb:/bin:/usr/bin''.


2.     For a native linker, search the contents of the environment variable "LD_LIBRARY_PATH".

3.     -bash: /home/mecr8410: Is a directory

------- TASK 2 --------

4.	prepend_path("PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/bin/intel64")
	prepend_path("PATH","/curc/sw/gcc/5.4.0/bin")
	prepend_path("LD_LIBRARY_PATH","/curc/sw/gcc/5.4.0/lib64")
	prepend_path("LD_LIBRARY_PATH","/curc/sw/intel/17.4/compilers_and_libraries_2017.4.196/linux/compiler/lib/intel64")

6.	Changes from:
	No modules loaded
	to:
	Currently Loaded Modules:
 	 1) intel/17.4 (m)

 	Where:
   	 m:  built for host and native MIC

7.	If two incompatible set of modules are loaded it can lead to module file conflict statemnts that will have to be updated. Software hierarchy makes only the modules that are built with the currently loaded compiler available. 

------- TASK 3 --------

8.	for intel:
 	
	pushenv("CC","icc")
	pushenv("FC","ifort")
	pushenv("CXX","icpc")
	pushenv("AR","xiar")
	pushenv("LD","xild")

	for gcc:
	
	pushenv("CC","gcc")
	pushenv("FC","gfortran")
	pushenv("CXX","g++")

------- TASK 4 --------

9.	Using built-in specs.
	COLLECT_GCC=gcc
	COLLECT_LTO_WRAPPER=/curc/sw/gcc/6.1.0/libexec/gcc/x86_64-pc-linux-gnu/6.1.0/lto-wrapper
	Target: x86_64-pc-linux-gnu
	Configured with: ../gcc-6.1.0/configure --prefix=/curc/sw/gcc/6.1.0 --enable-languages=c,c++,fortran,go --disable-multilib --with-tune=intel
	Thread model: posix
	gcc version 6.1.0 (GCC)

10.	g++ -c axpy.cc
	g++ -c test_axpy.cc
	g++ -o text_axpy.exe test_axpy.o axpy.o

------- TASK 5 --------

11.	squeue -u $USER
	
	I was unable to complete sbatch job.sh:
	sbatch: error: Batch job submission failed: Invalid account or account/partition combination specified
	
	instead I used:
	./test_axpy.exe

------- TASK 6  --------

12.	before it only returned:
	Calling axpy - Passed

	after applying the flag -fopenmp I got:
	Calling axpy - Passed
	Elapsed time: 0.78177s

13.	set to 1:
	Elapsed time: 1.51974s

	set to 2:
	Elapsed time: 1.23701s

	set to 4:
	Elapsed time: 0.928338s

	set to 8:
	Elapsed time: 0.634274s

	set to 16:
	Elapsed time: 0.543635s



