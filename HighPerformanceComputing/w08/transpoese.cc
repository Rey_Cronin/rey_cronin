#include "slurp_file.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>




typedef int data_t;
typedef std::vector<data_t> vec_t;
typedef std::vector<std::vector<data_t> > mat_t;


// if using matrix input, you need to count the number of lines
// to divide the rows to the processors
int countlines(const char* fname) {

    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile(fname);

    while (std::getline(myfile, line))
        ++number_of_lines;
    return number_of_lines;

}


void csr2coo(vec_t row, vec_t &rowc,int m) {
	vec_t().swap(rowc); // clear contents of rowc
	for(int i = 0; i < m; i++) {
		int a = row[i];
		int b = row[i+1];
		for(int j = a; j < b; j++) {
			rowc.push_back(i);
		}
	}
}

void coo2csr(vec_t rowc, vec_t &row, int n, int &m) {
	vec_t().swap(row); // clear contents of row
	row.push_back(0);
	for (int i = 0; i < n; i++) {
		if(rowc[i] != rowc[i+1]) {
			row.push_back(i);
		}
		if(i == n-1) {
			row.push_back(row.back()+1);
		}
	}
        m = row.size() - 1; // length(row) = m+1
	
}	

//void transpose(vec_t val  
			

int main(int argc, char** argv) {
	const char *filename = "CSR.txt";
	vec_t val;
	vec_t col;
	vec_t row; //csr row format 
        vec_t rowc; // row coordinates
	slurp_file_line(filename, 0, val);
        slurp_file_line(filename, 1, col);
        slurp_file_line(filename, 2, rowc);
	int m = rowc.back() // number of rows
	int n = val.size(); // number of values
	mat_t matrix(m, std::vector<data_t>(n,0));
	int q; // if n
	int rank, nproc;
	n = countlines(filename); // how many rows in matrix
	

        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Status status;

	q = n / nproc; // how many rows each processor gets
	extra = n % nproc; // some processors will get extra

	count = 0;
	for(int i = rank * q; i < (rank + 1) *q; i++) {
		vec_t rowp; // row being pulled
		mat_t rowsp; // collection of rows for processor
		slurp_file_line(filename, i, rowp);
		count2 = 0;
		for vec_t::const_iterator j = rowp.begin(); j != rowp.end(); ++j) {
			rowsp[count][count2] = j
			count2 = count2 + 1;
		}
		count = count + 1;
	}
	

	

for (std::vector<data_t>::const_iterator i = rowc.begin(); i != rowc.end(); ++i)

                        std::cout << *i << ' ';
                        std::cout << "\n";


	
	vec_t r;
	vec_t c;
	int column = 0;
	int count = 0;
	
	matrix[0][0] = 5;


        MPI_Finalize();


}	
	
