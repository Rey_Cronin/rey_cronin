#include "slurp_file.h"
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <mpi.h>




typedef int data_t;
typedef std::vector<data_t> vec_t;
typedef std::vector<std::vector<data_t> > mat_t;


// if using matrix input, you need to count the number of lines
// to divide the rows to the processors
int countlines(const char* fname) {

    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile(fname);

    while (std::getline(myfile, line))
        ++number_of_lines;
    return number_of_lines;

}


void csr2coo(vec_t row, vec_t &rowc,int m) {
	vec_t().swap(rowc); // clear contents of rowc
	for(int i = 0; i < m; i++) {
		int a = row[i];
		int b = row[i+1];
		for(int j = a; j < b; j++) {
			rowc.push_back(i);
		}
	}
}

void coo2csr(vec_t rowc, vec_t &row, int n, int &m) {
	vec_t().swap(row); // clear contents of row
	row.push_back(0);
	for (int i = 0; i < n; i++) {
		if(rowc[i] != rowc[i+1]) {
			row.push_back(i);
		}
		if(i == n-1) {
			row.push_back(row.back()+1);
		}
	}
        m = row.size() - 1; // length(row) = m+1
	
}	

//void transpose(vec_t val  
			

int main(int argc, char** argv) {
	/*
	const char *filename = "CSR.txt";
	vec_t val;
	vec_t col;
	vec_t row; //csr row format 
        vec_t rowc; // row coordinates
	slurp_file_line(filename, 0, val);
        slurp_file_line(filename, 1, col);
        slurp_file_line(filename, 2, rowc);
	int m = rowc.back() // number of rows
	int n = val.size(); // number of values
	mat_t matrix(m, std::vector<data_t>(n,0));
	*/
	const char *filename = "matrix.txt";
	int q; // if n
	int n; // number of rows
	int extra;
	int rank, nproc;
	n = countlines(filename); // how many rows in matrix
	vec_t check; // checking number of columns
	slurp_file_line(filename, 0, check);
	int m = check.size(); // number of columns

        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
        MPI_Status status;

	q = n / nproc; // how many rows each processor gets
	extra = n % nproc; // some processors will get extra

	// if the number of rows is less than nproc
	if( q == 0) {
                nproc = n;
                q = 1;
                extra = 0;
        }


	int count = 0;
	int count2 = 0;
	
	int low = rank * q; // lower bound
	if (rank > 0 && extra >= rank) {
		low = low + rank;
	}
	if (rank > 0 && extra < rank) {
		low = low + extra;
	}
        
	int up = low + q - 1; // upper bound
        if( rank < extra ) {
                up = up + 1;
        }
	int num_row = up - low + 1; // number of rows for that processor

	//std::cout << "rank: " << rank << " has upper " << up << " has lower " << low << " has a total of " << num_row << " lines\n";
	vec_t rowp; // row being pulled
	mat_t rowsp(num_row, std::vector<data_t>(m,0)); // collection of rows for processor
	mat_t rowst(m, std::vector<data_t>(num_row,0));
        if( rank == 0 ) {
                std::cout << "size is " << rowsp[0].size() << "\n";
        }


	// give each processor its rows in a vector of vectors
	for(int i = low; i <= up; i++) {
		vec_t().swap(rowp);
		slurp_file_line(filename, i, rowp);
		for (int j = 0; j < m; j++) {
			rowsp[count][j] = rowp[j];
                }
		count = count + 1;
	}

	vec_t b(rowp.size());
	
	for (std::vector<data_t>::const_iterator i = rowp.begin(); i != rowp.end(); ++i)

                        std::cout << *i << ' ';
                        std::cout << "\n";
		


	//MPI_Alltoall(&rowp[0], 1, MPI_INT, &b[0], 1, MPI_INT, MPI_COMM_WORLD);

	//MPI_Alltoall(&rowsp[0][0], num_row, MPI_INT, &rowst[0][0], num_row, MPI_INT, MPI_COMM_WORLD);
	
	
	// transpose
	for(int i = 0; i < rowsp.size(); i++) {
		for(int j = 0; j < rowsp[i].size(); j++) {
			rowst[j][i] = rowsp[i][j];
		}
	}
	
	/*
	// all the processes send to one processor
	if(rank > 0) {
		for(int i = 0; i < num_row; i++) {
			MPI_Send(		
	*/







		
	if( rank == 3 ) {
		for (int i = 0; i <rowsp.size(); i++) {
			for ( int j = 0; j < rowsp[i].size(); j++) {
				std::cout << rowsp[i][j] << " ";
			}
		std::cout << "\n";
		}
		for (int i = 0; i <rowst.size(); i++) {
                        for ( int j = 0; j < rowst[i].size(); j++) {
                                std::cout << rowst[i][j] << " ";
                        }
                std::cout << "\n";
                }

	}
	


/*
mat_t m_image_data2(5, std::vector<data_t>(2,0));

if (rank>0) {
  m_image_data2[0][1] = 1;
  m_image_data2[1][1] = 2;
  m_image_data2[2][1] = 3;
  m_image_data2[3][1] = 4;
  m_image_data2[4][1] = 5;

                for (int i = 0; i <m_image_data2.size(); i++) {
                        for ( int j = 0; j < m_image_data2[i].size(); j++) {
                                std::cout << m_image_data2[i][j] << " ";
                        }
                std::cout << "\n";
                }


  // send 5 ints at once
  MPI_Send( &m_image_data2[0][0], 10, MPI_INT, 0, 0, MPI_COMM_WORLD);
}
else {
  // make space for 5 ints
  mat_t m_image_data2(5, std::vector<data_t>(2,0));

	// receive 5 ints
  MPI_Recv(&m_image_data2[0][0], 10, MPI_INT, 1, 0, MPI_COMM_WORLD, &status);


                for (int i = 0; i <m_image_data2.size(); i++) {
                        for ( int j = 0; j < m_image_data2[i].size(); j++) {
                                std::cout << m_image_data2[i][j] << " ";
                        }
                std::cout << "\n";
                }

}
	
*/
	
	
/*
for (std::vector<data_t>::const_iterator i = rowc.begin(); i != rowc.end(); ++i)

                        std::cout << *i << ' ';
                        std::cout << "\n";
*/

	
	vec_t r;
	vec_t c;
	int column = 0;
	


        MPI_Finalize();


}	
	
