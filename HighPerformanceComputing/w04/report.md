------ TASK 3 ------

3. List the variables that are subject to race condition and describe the potential side effect of race condition.
	
	- idx 
	- next_put
	- next_get	
	
	- two processors could be accessing idx at the same time
	- similarly they could be incrimenting idx simultaneously
		- This would cause:
			- multiple producers could write multiple products
			  to the same index in the buffer causing overwrites
			- multiple consumers could grab the same product
			  causing over counting

4. We are not linking semaphore-ar.obj which is how the producers and consumers are able to be synchornized. It is a similar case to when we compiled ./pc.exe 10 1 1 except we are doing 500 iterations.

 
