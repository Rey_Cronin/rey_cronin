g++ -c -fopenmp -fpermissive -std=c++11 quicksort.cc test_quicksort.cc;
g++ -o test_quicksort.exe -fopenmp -fpermissive -std=c++11 quicksort.o test_quicksort.o;


OMP_NUM_THREADS=1 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=2 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=3 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=4 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=5 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=6 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=7 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=8 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=9 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=10 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=11 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=12 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=13 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=14 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=15 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=16 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=17 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=18 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=19 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=20 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=21 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=22 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=23 ./test_quicksort.exe 1000000
OMP_NUM_THREADS=24 ./test_quicksort.exe 1000000


