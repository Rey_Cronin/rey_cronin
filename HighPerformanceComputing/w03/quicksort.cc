/*
 * * @input x the array to be sorted
 * * @output y the output array (already allocated)
 * */
//http://www.algolist.net/Algorithms/Sorting/Quicksort

#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>
#include <iostream>
#include <vector>
#include <iterator>

typedef std::vector<long> vec_t;


void quicksort(const vec_t &x, vec_t &y)
{

	int len = x.size(); //length of array
        int len2 = len - 1; // length not including pivot

	y.resize(len);


	if (x.size() < 2) {
	        y = x;
		return;
	}

	vec_t xl, xu, yl, yu;
	vec_t bil(len2); //binary lower
        vec_t biu(len2); // binary upper
	vec_t accl(len2); // accumalative sum lower
	vec_t accu(len2); // accumalative sum upper
	int countl = 0;
	int countu = 0;	

	
	int pivot = x[len -1]; // pivot is set to last index in array

	// creating a binary array that says if it is in lower or upper	


	#pragma omp parallel
	{
	#pragma omp for
	for (int i = 0; i < len2; i++)
	{
		if(x[i] <= pivot) {
			bil[i] = 1;
		}else {
			biu[i] = 1;
		}
	}
	}

	
	// creating the accumulative array
	// this is sequential bc of count
	for (int i = 0; i < len2; i++) {
		if(bil[i] == 1){
			countl += 1;
		}
		accl[i] = countl;
		if(biu[i] == 1) {
			countu += 1;
		}
		accu[i] = countu;
	}

	int ll = accl[len2-1]; // length of lower
        int lu = accu[len2-1]; // lenght of upper



	xl.resize(ll); 
	xu.resize(lu);
	yl.resize(ll); 
	yu.resize(lu);


        #pragma omp parallel
        {
        #pragma omp for	
	for (int i = 0; i < len2; i++) {

		if(bil[i] == 1) {
			xl[accl[i]-1] = x[i]; 
		}
		
		if(biu[i] == 1) {
			xu[accu[i]-1] = x[i];
		}
	}
	}
		
	
	
        #pragma omp parallel
        {
	#pragma omp single 
	{		
	#pragma omp task shared(yl)	
	quicksort(xl, yl);	
	#pragma omp task shared(yu)
	quicksort(xu, yu);
	}
	}


        #pragma omp parallel
        {
        #pragma omp for
	for(int i = 0; i < ll ; i++) { 
		y[i] = yl[i];
	}

	y[ll] = pivot;

        #pragma omp for
	for(int j = 0; j < lu ; j++) {
		y[j+ll+1]= yu[j];
	}
	}	 



}
// creating a cutoff when the array is too big so that it works sequentially










