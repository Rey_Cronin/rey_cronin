#include<stdio.h>
#include<omp.h>

long fib(long n) {
    long i(0), j(0);
    if (n<3) return 1;

    #pragma omp task shared(i)
    i = fib(n-1);

    #pragma omp task shared(j)
    j = fib(n-2);

    /* ... more code ... */
    	#pragma omp taskwait
	return i + j;
}




int main(){

    long n(13), v;
    #pragma omp parallel shared (n, v)
    {
        #pragma omp single /* why single? */
        {
	v = fib(n);
	printf("fib(%d) = %d\n", n, fib(n));
	}
    }
}


