/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 *
 * @brief test driver for Possion
 */
#include <mpi.h>
#include "poisson.h"
#include <iostream>
#include <math.h>
typedef std::vector<point_t> grid_t;


void print_vect(std::vector<real_t> s){
        std::cout<< "the vector is: " <<std::endl;
        for (std::vector<real_t>::const_iterator i = s.begin(); i != s.end(); ++i)
        std::cout << *i << ' ';
        std::cout<<"\n";
}


void function( grid_t &x, vec_t &a, vec_t &f, vec_t &v) {
	const double pi = 3.1415;

	
	for(std::vector<point_t>::const_iterator i = x.begin(); i != x.end(); ++i){
		std::cout<< "flag";
		a.push_back(12);
		f.push_back(sin(4*pi*i->x)*sin(10*pi*i->y)*sin(14*pi*i->z)*(12+312*pi*pi));
		v.push_back((sin(4*pi*i->x)*sin(10*pi*i->y)*sin(14*pi*i->z)));
	}


}




int main(int argc, char** argv){

    /** add initialization, get n from command line */

        int rank,nproc;


	MPI_Init(&argc, &argv);

    MPI_Comm grid_comm;
    int n = atoi(argv[1]);
    grid_t x;
    poisson_setup(MPI_COMM_WORLD, n, grid_comm, x);

        MPI_Comm_size(MPI_COMM_WORLD, &nproc);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);


	
	
    /** fill a and f based on x **/
    vec_t a, f;

//	poisson_matvec(grid_comm, n, a, v, lv)


    // make a matvec object to pass to the residual function (residual
    // doesn't care how you do the matvec, it just passes an input and
    // expect and output.
    matvec_t mv = std::bind(poisson_matvec, std::ref(grid_comm), n, std::ref(a), std::placeholders::_1, std::placeholders::_2);

    // now you can call mv(v,lv)
    vec_t v, rhs, res;
    real_t res_norm;

	vec_t lv;
      poisson_matvec(grid_comm, n, a, v, lv);

    residual(MPI_COMM_WORLD, mv, v, rhs, res, res_norm);
    //std::cout<<"Residual norm: "<<res_norm<<std::endl;

    /** cleanup **/
    return 0;
}



