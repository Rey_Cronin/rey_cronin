/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision: 25 $
 * @tags $Tags: tip $
 * @date $Date: Thu Jan 01 00:00:00 1970 +0000 $
 */
#include <mpi.h>
#include "poisson.h"
#include <iostream>
#include <math.h>
#include <stdio.h>


/*
 * Set up the grid communicator for the Poison problem and samples
 * the correct portion of the domain
 * 
 * @comm input communicator, typically MPI_COMM_WORLD
 * @n problem size, there are n sample points along each axis
 * @grid_comm the grid communicator (output)
 * @x sample points asigned to the current MPI proces based on
 * its grid coordsinates (output)
*/



void printarray(int arg[], int length) {
  for (int n=0; n<length; ++n)
    std::cout << arg[n] << ' ';
        std::cout << '\n';
}




// dims - number of nodes in each dimension
// grid_comm - the grid communicator
void poisson_setup(const MPI_Comm &comm, int n, MPI_Comm &grid_comm,grid_t &x)
{

		
	int size;
    	int rank;
	int ndims = 3;
	int dims[ndims] = {0, 0, 0}; // the number of proceses in each dimension
	int periods[ndims] = {0, 0, 0};
	int reorder = 1;
	int coords[ndims] = {0,0,0};
	
	// Determines the size of the group asociated with a communicator
	MPI_Comm_size(comm, &size);
	        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	

	//std::cout <<"n is: "<< n << std::endl;


	// get dimensions - number of nodes in each dir
	MPI_Dims_create(size, ndims, dims);
	// create grid structure
	MPI_Cart_create(comm, ndims, dims, periods, reorder, &grid_comm);
	// gets proces coordsinates
	MPI_Cart_coords(grid_comm, rank, ndims, coords);

	//std::cout<< "the rank is: " << rank << std::endl;
        //std::cout<< "the ndims is: " << ndims<< std::endl;
//        std::cout<< "the coordsinates are: " <<std::endl;
        printarray(coords, 3);
//	std::cout<< "the coordsinates are: " <<coords[3] <<std::endl;
//	std::cout<< "the dims are: " <<std::endl;

//	printarray(dims,3);

	// get the interval in i,j,k directions
	
	int int_i = n/dims[0];
	int int_j = n/dims[1];
	int int_k = n/dims[2];
	int count = 0;
	for(real_t i = coords[0]*int_i; i < coords[0]*int_i+int_i; i++){
		for(real_t j = coords[1]*int_j; j < coords[1]*int_j+int_j; j++){	
			for(real_t k = coords[2]*int_k; k < coords[2]*int_k+int_k; k++){
				
				point_t point = {i,j,k};
				x.push_back(point);
				count = count + 1;

			}
		}
	}
	std::cout<<"count is: "<< count<<'\n';
	  std::cout << "size of x:  " << x.size() << '\n';



	//x.push_back(1);
	//x[1] = 1;
}

/*
 * @grid_comm grid communicator returned by poisson_setup - communicator with new cartesisan topology
 * @a value of coefficient a at points x
 * @v candidate solution at points x
 * @lv the matrix-vector product of the discrete Poisson's operator
 */






void poisson_matvec(const MPI_Comm &grid_comm, int n, const vec_t &a, const vec_t &v, vec_t &lv)
{



	        int size;
        int rank;
        int ndims = 3;
        int dims[ndims] = {0, 0, 0}; // the number of proceses in each dimension
        int periods[ndims] = {0, 0, 0};
        int reorder = 1;
        int coords[ndims] = {0,0,0};

        // Determines the size of the group asociated with a communicator
        MPI_Comm_size(grid_comm, &size);
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	//get coords
	MPI_Cart_coords(grid_comm, rank, ndims, coords);

	// number of data points that will be passes between
	int num = cbrt(a.size() * size);
        std::cout<<"num is : "<< num <<'\n';

	/*int x_int = num/dims[0];
	int y_int = num/dims[1];
	int z_int = num/dims[2];
        std::cout<<"x_int is : "<< "what" <<'\n';
	*/



	MPI_Status status;


}




/*
 * @comm mpi communicator
 * @mv the matvec operator
 * @v the input vector to matvec
 * @rhs the right-hand-side of the linear operator
 * @res the point-wise residual (output)
 * @res_norm the L2 norm of the residual vector `res`
 */


void residual(const MPI_Comm &comm, matvec_t &mv, const vec_t &v, const vec_t &rhs, vec_t &res, real_t &res_norm)
{

	
  //  std::cout<<"residual"<<std::endl;
}


/*
void print_vect(std::vector<real_t> s){
        std::cout<< "the vector is: " <<std::endl;
        for (std::vector<char>::const_iterator i = s.begin(); i != s.end(); ++i)
        std::cout << *i << ' ';
        std::cout<<"\n";
}
*/




