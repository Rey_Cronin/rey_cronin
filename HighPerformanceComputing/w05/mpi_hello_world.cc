#include <iostream>
#include <mpi.h>

int main(int argc, char** argv){

	MPI_Init(&argc, &argv);
	
	int rank, nproc;
	// returns the size of a communicator
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
	
	// returns the rank of a porcessi n a communicator
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);



	if (rank == 0){
		std::cout<<"Hello from processor 0 of 4\n";
	}
        if (rank == 1){
                std::cout<<"Hello from processor 1 of 4\n";
        }
        if (rank == 2){
                std::cout<<"Hello from processor 2 of 4\n";
        }
        if (rank == 3){
                std::cout<<"Hello from processor 3 of 4\n";
        }


	MPI_Finalize();
}
