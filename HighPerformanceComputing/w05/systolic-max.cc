#include <vector>
#include "slurp_file.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iostream>
#include <mpi.h>

typedef int data_t;
typedef std::vector<data_t> vec_t;

int get_max(vec_t s) {

	int max = s[0];

	for(int i = 1; i < s.size(); i++) { 
		if (s[i] > max) {
			max = s[i];
		}
	}
	return max;
}


// get max of two integers
int max_two(int a, int b) {

	int max = a;

	if (b > a) {
		max = b;
	}
	return max;
}
		


int main(int argc, char** argv){
	
    	int rank,nproc;
	int global_max, my_max, local_max;
	const char *filename = "array.txt";
	vec_t data;
	int max;
	int right_max;

        MPI_Init(&argc,&argv);	
    	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Status status;
                
	slurp_file_line(filename, rank, data);

	local_max = get_max(data);	


	/*

	MPI_Send(&local_max, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
	MPI_Recv(&right_max, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	global_max = max_two(right_max,local_max);

	if (rank == 0) {
		std::cout << "the global max is: " << global_max;
	}
	*/
	

	if (rank == nproc-1) {
		 MPI_Send(&local_max, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
	}
	
	if (rank > 0 && rank < nproc-1) {
		MPI_Recv(&max, 1, MPI_INT, rank + 1 ,0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		max = max_two(local_max,max);			
		MPI_Send(&max, 1, MPI_INT, rank - 1 ,0, MPI_COMM_WORLD);
	}

	

	
	if (rank == 0) {
		MPI_Recv(&max, 1, MPI_INT, 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		max = max_two(local_max, max);
		std::cout << "the global max is: " << max << "\n";

	}


	MPI_Finalize();


}

