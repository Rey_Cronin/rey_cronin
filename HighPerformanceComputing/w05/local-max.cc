#include <vector>
#include "slurp_file.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iostream>
#include <mpi.h>

typedef int data_t;
typedef std::vector<data_t> vec_t;

int get_max(vec_t s) {

	int max = s[0];

	for(int i = 1; i < s.size(); i++) { 
		if (s[i] > max) {
			max = s[i];
		}
	}
	return max;
}



int main(int argc, char** argv){

    	int rank,nproc;
	int global_max, my_max, local_max;
	const char *filename = "array.txt";
	vec_t data;

        MPI_Init(&argc,&argv);	
    	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Status status;
                
	slurp_file_line(filename, rank, data);

	std::cout << "the max of line " << rank << " is " << get_max(data) << " determined by processor " << rank;
        std::cout << "\n";

	MPI_Finalize();

}

