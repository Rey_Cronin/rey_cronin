#include <vector>
#include "slurp_file.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iostream>
#include <mpi.h>

typedef int data_t;
typedef std::vector<data_t> vec_t;

		
int countlines(const char* fname) {

    int number_of_lines = 0;
    std::string line;
    std::ifstream myfile(fname);

    while (std::getline(myfile, line))
        ++number_of_lines;
    return number_of_lines;

}


vec_t col_element_mult(vec_t v, int el, int n) {
	vec_t c(v.size());
 
	for( int i = 0; i < v.size(); i++) {
		c[i] = v[i] * el;
	}

	return c;
}



int main(int argc, char** argv){
	
	const char *filename = "matrix.txt";
	vec_t vector;
	int v_element;
	vec_t c;
	vec_t cc;
	//vec_t mat_col;
	int q; // normal number of columns each processor will have to take
	int extra; // some processors will have to take extra columns
	int n; 
	int rank,nproc;
	int add;
	MPI_Init(&argc, &argv);	
    	MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	MPI_Status status;
                
	n = countlines(filename)-1;
	slurp_file_line(filename,n, vector); // get the vector
	q = n / nproc;
	extra = n % nproc;

	if( q == 0) {
		nproc = n;
		q = 1;
		extra = 0;
	}
	      
	vec_t first_line;
	slurp_file_line(filename, 1, first_line);
        int len = first_line.size();
        c.resize(n);
	 

	
	for(int i = rank * q; i < (rank + 1) * q; i++) {


		vec_t mat_col;
		vec_t ccc;
		slurp_file_line(filename, i, mat_col);

		v_element = vector[i];
		ccc = col_element_mult(mat_col, v_element, n);
		

                for( int i = 0; i < n; i++) {
                	c[i] += ccc[i];
                }
	
	}

	if (rank < extra) {
        	for(int i = rank * q + nproc ; i < (rank + 1) * q + nproc; i++) {
                	vec_t mat_col2;
			slurp_file_line(filename, i, mat_col2);
                	v_element = vector[i];
                	cc = col_element_mult(mat_col2, v_element, n);
			
				for( int i = 0; i < n; i++) {
					c[i] += cc[i];
				}
        	}
	}



	if( nproc == 1) {
	        MPI_Finalize();
		std::cout << "A * x = ";
                for (auto i = c.begin(); i != c.end(); ++i)
                        std::cout << *i << ' ';
                        std::cout << "\n";

	}else {

	if (rank == nproc-1) {
		for(int i = 0; i < n; i++) { 
			MPI_Send(&c[i], 1, MPI_INT, rank - 1, i, MPI_COMM_WORLD);
		}
	}
	
	if (rank > 0 && rank < nproc-1) {
		for( int i = 0; i < n; i++) {
			MPI_Recv(&add, 1, MPI_INT, rank + 1 ,i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			c[i] = add + c[i];			
			MPI_Send(&c[i], 1, MPI_INT, rank - 1 ,i, MPI_COMM_WORLD);
		}
	}

	if (rank == 0) {
                for( int i = 0; i < n; i++) {
			MPI_Recv(&add, 1, MPI_INT, 1, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			c[i] = add + c[i];
		}
		
		std::cout << "A * x = ";		                       
		for (auto i = c.begin(); i != c.end(); ++i)
			std::cout << *i << ' ';
			std::cout << "\n";

	}

	MPI_Finalize();

	}



}

