/**
 * @file
 * @author Rahimian, Abtin <arahimian@acm.org>
 * @revision $Revision$
 * @tags $Tags$
 * @date $Date$
 *
 * @brief
 */

#include "quicksort.h"
#include <cmath>
#include <cassert>
#include "omp-utils.h"

#include <algorithm>

/*
 * Threashold values to switch between work sharing (NWS) or task
 * parallelism. Should be tuned for each machine.
 */

#define NWS 5e4  /* minimum size for workshare */
#define NTSK  5e4  /* size to switch to tasks    */
#define NMIN 50 /* minimum size for fial task */

void scan_seq(int *x, size_t n){
    for(int ii=1; ii<n; ++ii) x[ii]+=x[ii-1]; // accumulation
}

void scan_par(int *x, size_t n){

    if (n<2) return;
    const int np=omp_get_max_threads();
    if (n<=np){
        for(int i(1);i<n;++i) x[i]+=x[i-1]; // accumaltive sum if the lenght of the vector is less than number of processeors
        return;
    }

    size_t *ps=new size_t[np]; // array the length of processors
    #pragma omp parallel default(none) firstprivate(x,n,ps)
    {
        size_t stride = floor(1.0*n/np); // stride gives the number of array points per processor
        int r         = omp_get_thread_num(); // gets the current thread number
        size_t offset = r*stride; // gets the offset in the array
        if (r==np-1) stride=n-offset; // gives the last processor all the extra if it's not evenly distributed

        for (size_t i=1;i<stride;++i)
            x[offset+i]+=x[offset+i-1]; 
        ps[r] = x[offset+stride-1];

	// barrier to wait for all other threads to have completed their task befoere moving on
        #pragma omp barrier
        #pragma omp single
        for (int i=1;i<np;++i)
            ps[i]+=ps[i-1];

        #pragma omp barrier
        if (r>0)
            for (size_t i=0;i<stride;++i)
                x[offset+i]+=ps[r-1];
    }
    delete[] ps;
}

void partition(const long *x, const size_t n, const long p, long *&L, size_t &lsz, long *&H, size_t &hsz){

    int *c = new int[n];
    int *l = new int[n];
    int *h = new int[n];

	// I think this is making a binary array
    #pragma omp parallel for default(none) firstprivate(c,l,h,x) if(n>NWS)
    for (size_t ii=0; ii<n; ++ii){
        l[ii] = c[ii] = x[ii]<=p; // ** this is confusing! if p is greater than x[ii] give a value in the lower
        h[ii] = !l[ii]; // make sure that value is the opposite for the high
    }

    // function pointer to scan
    typedef void (*scan_t)(int*,size_t);
    scan_t scan = (n>NWS) ? scan_par : scan_seq; // ** if size of array is greater than NWS scan_seq?

    scan(l,n); // ** l is the accumalative sum array
    scan(h,n);

    lsz=l[n-1]; // gives size of the low
    hsz=h[n-1]; 
    assert (lsz+hsz==n);

    if (lsz>0) L = new long[lsz];
    if (hsz>0) H = new long[hsz];

    long* Lp(L), *Hp(H);
    #pragma omp parallel for default(none) firstprivate(l,h,c,x,Lp,Hp) if(n>NWS)
    for (size_t ii=0; ii<n; ++ii)
        if (c[ii]) Lp[l[ii]-1]=x[ii]; // ** if c[ii] = 1 ? then put x[ii] in the lower partition
        else       Hp[h[ii]-1]=x[ii];

    delete[] c;
    delete[] l;
    delete[] h;

    return;
}

void quicksort_tsk(const long *x, size_t n, long *y){

    if (n<2) {
        for (size_t ii=0;ii<n;++ii) y[ii]=x[ii];
        return;
    }

    long p = x[n-1];
    long *L(NULL), *H(NULL);
    size_t lsz, hsz;
    partition(x,n-1,p,L,lsz,H,hsz);
    y[lsz] = p;

    #pragma omp task default(none) firstprivate(L,lsz,y) if(n>NMIN) // ** what happens when n < NMIN??
    {
        quicksort_tsk(L,lsz,y);
        delete[] L;
    }

    #pragma omp task default(none) firstprivate(H,hsz,lsz,y) if(n>NMIN)
    {
        quicksort_tsk(H,hsz,y+lsz+1);
        delete[] H;
    }

    return;
}

void quicksort(const long *x, size_t n, long *y){
	// if size of array is 1 or 0 you put it in sorted
    if (n<2) {
        for (size_t ii=0;ii<n;++ii) y[ii]=x[ii];
        return;
    }
	// if n is less than NTSK, use tasks for quicksort
    if (n<NTSK){
        #pragma omp parallel default(none) firstprivate(x,n,y)
        {
            #pragma omp single nowait
            quicksort_tsk(x,n,y);
        }
        return;
    }
	// if n is greater than NTSK, use regular quickssort
	// partition is last value in vector
    long p = x[n-1];
	// low and high vectors
    long *L(NULL), *H(NULL);
	// size of low and hig vectors
    size_t lsz, hsz;
	// partition x around p
	// inputs are vector x, length x, low vector, low size, high vector, high size
    partition(x,n-1,p,L,lsz,H,hsz);
    y[lsz] = p; // putting partition in after the low vector (between high and low

    quicksort(L,lsz,y); // recursive call of the quicksort of the new low vector
    quicksort(H,hsz,y+lsz+1); // ** what does y+lsz+1 mean??

    delete[] L;
    delete[] H;
}
