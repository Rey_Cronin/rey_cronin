g++ -c -fopenmp -fpermissive -std=c++11  quicksort.cc test_quicksort.cc;
g++ -o test_quicksort.exe -fopenmp -fpermissive -std=c++11 quicksort.o test_quicksort.o;

OMP_NUM_THREADS=24 ./test_quicksort.exe 1000000


# Strong scaling
#for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe 10000000;done
# Weak scaling
#for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((10000000*$i));done

#for i in {1..24};do export OMP_NUM_THREADS=$i; ./test_quicksort.exe $((100000*$i));done

for i in {1,2,4,8,12,16,20,24};
do
        OMP_NUM_THREADS=$i ./test_quicksort.exe $((i*10000000)) --cpus-per-task 24;
done

echo " "



