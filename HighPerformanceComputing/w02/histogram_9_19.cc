#include<iterator>
#include<algorithm>
#include <cstddef>
#include <vector>
#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>
#include <iostream>

//int  n=10 /* or 1e7 */, num_bins=3;
//vec_t x(n), bin_bdry;
//count_t bin_count;


/* read x from file, etc. */

typedef std::vector<double> vec_t;


void histogram(const vec_t &x, int num_bins,          /* inputs  */
                   vec_t &bin_bdry, count_t &bin_count)  /* outputs */
{
	int i;
	int j;
	int bin_tot;
//	double mx; // = x[0];
//	double mn; // = x[0];
//	mx = *std::max_element(x.begin(), x.end());
//	mn = *std::min_element(x.begin(), x.end());

	double mx = x[0];
	double mn = x[0];



	
	#pragma omp parallel for reduction(min:mn)
	for (i = 1;i < x.size(); i++)
	{
		mn = std::min(mn,x[i]);
	}
        #pragma omp parallel for reduction(max:mx)
        for (i = 1;i < x.size(); i++)
        {
                mx = std::max(mx,x[i]);
        }

	
	int number_boundaries = num_bins + 1; //number of boundaries
	
	vec_t vv(number_boundaries);
	
	bin_bdry=vv;

        bin_bdry[0] = mn*(1-1E-14); // minimum boundary begins slightly before min value
        bin_bdry[num_bins] = mx*(1+1E-14); // bin boundary ends slightly after max value

	double step = (bin_bdry[num_bins]-bin_bdry[0]) / num_bins; // how much space between each bin


	// filling a vector with all the bin boundaries
	#pragma omp parallel shared(bin_bdry)
	{
		#pragma omp for
		for (i = 1; i < number_boundaries; i++)
		{
			bin_bdry[i] = bin_bdry[0]+i*step;//*(1-1E-14);
		}	
	}

                for (auto i = bin_bdry.begin(); i != bin_bdry.end(); ++i)
                std::cout << *i << ' ';


 // this is the vect r that has how many items are in each bin
	
	// vv2(num_bins);
	count_t vv2(num_bins);
	bin_count = vv2; 
	//bin_count.resize(num_bins);
	
	const int t_max = omp_get_max_threads();
        //count_t bc_threads(num_bins*t_max); 
	const int nb = num_bins;


	int initial_value = 0;
	std::vector< std::vector<size_t> > bc_threads;
	bc_threads.resize( nb , std::vector<size_t>( t_max, initial_value ));






	
       #pragma omp parallel
        {
                const int thread_i = omp_get_thread_num(); // what thread are you on?
		
		 printf("thread num= %d\n",thread_i);		

                #pragma omp for
                for (i = 0; i < x.size(); ++i)
                {
                        // which bin should I put x[i] in?

                        int bin = std::floor((x[i]-bin_bdry[0])/step);
                        bc_threads[thread_i][bin] += 1;
                }
        //      #pragma omp single
        //      {
        //      for (auto i = bc_threads.begin(); i != bc_threads.end(); ++i)
        //      std::cout << *i << ' ';

        //      int sum = accumulate(bc_threads.begin(), bc_threads.end(), 0);
        //      printf("%d\n",sum);
        //      }

		#pragma omp single
	        {
		for (int i = 0; i < bc_threads.size(); i++)
	        {
        	        for (int j = 0; j < bc_threads[i].size(); j++)
                	{
                	std::cout << bc_threads[i][j] <<' ';
               		}
        	}

		printf("\n");
		}



                #pragma omp for reduction(+:bin_tot)
                for (i = 0; i < num_bins; i++)
                {
                        bin_tot=0;
                        for (j = 0; j < t_max; j++)
                        {
                                bin_tot += bc_threads[j][i];
                        }

                        bin_count[i]=bin_tot;

		}

                #pragma omp single
                {
                        for (auto i = x.begin(); i != x.end(); ++i)
                        std::cout << *i << ' ';

                        int sum = accumulate(bin_count.begin(), bin_count.end(), 0);
                        printf("total count = \n%d\n",sum);

                }

        }






}

