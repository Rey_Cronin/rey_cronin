1. find the max and min values of the ata set (the vector)

2. define the boundaries for each bin. this cna be done by (max-min)/#bin
3. now you want to assign the numbers in the data set to the corresponding bin and increase the counter
	
	since the data is uniformly dist you can simply map the min manx range 
	to bin range




----------- TASK 1 -----------

PART 1

1. We use reduction to find the min/max of the array x. Each thread does a partial reduciton and the final output is the combined reductions.

2. We find the number of boundaries by taking the inputted number of bins plus one.

3. We find how much space is between each bin by finding the difference between the min and max and dividing by the given number of bins.

4. Assigning a vector with the bin boundaries can be parallelized by taking the for loop and letting the appropiate amount of threads take on assigning the boundary of the bin[i] by mulitplying i by the space between each bin.

5. Parallelizing the count of how many vector elements from vector x go in each individual bin is the most difficult to parallize. More than one vector element can go into a certain one bin. If both thread a and b simultaneously want to count two different x elements into bin 1, then one of the counts could be unrepresented in the count. This is called race conditions and can be avoided by allowing access to a specific count of a bin to be atomic. Atomic access to a memomry location can be implemented using #pragma omp atomic. Another way is to use a private array for each thread to contribute its count to and then sum the results in the end.

PART 2

Wall-clock time (T) - Actual time elapsed from the start of a program

Speed up (S) - Ratio of the best sequential time T_1 to the time on p processors T_p 

Efficiency (E) - Speedup/number of proccessors

Scalability -fixed size (fixed problem size) or isogranular (fixed work per processor)





----------- TASK 2 -----------



	
https://pdfs.semanticscholar.org/4e60/37127e85445079272e1cb5574cbcce2e175e.pdf

http://forum.openmp.org/forum/viewtopic.php?f=3&t=70:wq



