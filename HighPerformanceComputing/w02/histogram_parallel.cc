#include<iterator>
#include<algorithm>
#include <cstddef>
#include <vector>
#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>
#include <iostream>

int  n=10 /* or 1e7 */, num_bins=3;
vec_t x(n), bin_bdry;
count_t bin_count;


/* read x from file, etc. */

typedef std::vector<double> vec_t;


void histogram(const vec_t &x, int num_bins,          /* inputs  */
                   vec_t &bin_bdry, count_t &bin_count)  /* outputs */
{
	int i;
//	double mx; // = x[0];
//	double mn; // = x[0];
//	mx = *std::max_element(x.begin(), x.end());
//	mn = *std::min_element(x.begin(), x.end());

	double mx = x[0];
	double mn = x[0];
	
	#pragma omp parallel for reduction(min:mn)
	for (i = 1;i < x.size(); i++)
	{
		mn = std::min(mn,x[i]);
	}
        #pragma omp parallel for reduction(max:mx)
        for (i = 1;i < x.size(); i++)
        {
                mx = std::max(mx,x[i]);
        }

	
	int number_boundaries = num_bins + 1; //number of boundaries
	
	vec_t vv(number_boundaries);
	
	bin_bdry=vv;


	double step = (mx - mn) / num_bins; // how much space between each bin

        bin_bdry[0] = mn*(1-1E-14); // minimum boundary begins slightly before min value

	// filling a vector with all the bin boundaries
	#pragma omp parallel shared(bin_bdry)
	{
		#pragma omp for
		for (i = 1; i < number_boundaries; i++)
		{
			bin_bdry[i] = i*step;
		}	
	}

	bin_bdry[num_bins] = mx*(1+1E-14); // bin boundary ends slightly after max value
	

	// this is the vector that has how many items are in each bin
	
	count_t vv2(num_bins);
	 
	bin_count = vv2;
	
	// filling a vector with all the counts for each bin
	#pragma omp parallel
	{	
		#pragma omp for
		for (i = 0; i < x.size(); ++i)
		{
			// which bin should I put x[i] in?
			if (x[i] == 0.0)
			{
				int bin = 0;
//				#pragma omp atomic
                                bin_count[bin] = bin_count[bin] + 1;			
			}
			else
			{
				int bin = std::ceil(x[i]/step)-1;
//				#pragma omp atomic
                                bin_count[bin] = bin_count[bin] + 1;

			}
	
		}		
	}	
//int bin = (x[i]) / (num_bins/10) *10  ; // you need to down (start at 0)
		
}



