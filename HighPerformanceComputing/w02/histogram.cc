#include "histogram.h"
#include<iterator>
#include<algorithm>
#include <cstddef>
#include <vector>
#include "histogram.h"
#include "omp-utils.h"
#include <cstdio>
#include <cmath>   //power
#include <cstdlib> //rand
#include <cassert>
#include <iostream>



void histogram(const vec_t &x, int num_bins, vec_t &bin_bdry, count_t &bin_count) {


        double mx = x[0];
        double mn = x[0];

        #pragma omp parallel for reduction(min:mn)
        for (int i = 1;i < x.size(); i++)
        {
                mn = std::min(mn,x[i]);
        }
        #pragma omp parallel for reduction(max:mx)
        for (int i = 1;i < x.size(); i++)
        {
                mx = std::max(mx,x[i]);
        }

    double step = (mx - mn) / num_bins;

    bin_bdry.resize(num_bins+1);
    bin_bdry[0]=mn*(1-1E-14);
    bin_bdry[num_bins]=mx*(1+1E-14);

    	#pragma omp parallel for
	for(int i=1; i < num_bins; i++) {
        	bin_bdry[i] = bin_bdry[i-1] + step;
    	}

    bin_count = count_t(num_bins);

    int num_threads = omp_get_max_threads();
    count_t bc_threads(num_threads*num_bins);

    #pragma omp parallel
    {
        int thread_i = omp_get_thread_num();

        #pragma omp for
        for (int i = 0; i < x.size(); i++) {
            int bin = std::floor((x[i] - bin_bdry[0]) / step);
            if(bin < 0) { bin = 0; }
	    if(bin > num_bins - 1) {bin = num_bins -1;}
	    int ind = thread_i*num_bins + bin;
            bc_threads[ind] += 1;
        }

        #pragma omp for
	for(int i=0; i<num_bins; i++) {
        	bin_count[i] = 0;
		for(int j=0; j<num_threads; j++) {
                	bin_count[i] += bc_threads[j*num_bins + i];
            	}	
        }
    }
//	bin_count[num_bins-1] += 1; //add final value
}
