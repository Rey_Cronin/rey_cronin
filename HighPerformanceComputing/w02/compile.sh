g++ -c -fopenmp -fpermissive -std=c++11  histogram.cc test_histogram.cc; 
g++ -o test_histogram.exe -fopenmp -fpermissive -std=c++11 histogram.o test_histogram.o;


for i in {1..24}
do
	OMP_NUM_THREADS=$i ./test_histogram.exe 5 1;
done
